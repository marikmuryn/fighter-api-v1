# ![Fighter-API](http://www.fightersgeneration.com/hr.gif) Fighter-API 

### Реалізовано веб-сервер  **Node.js** + **Express.js**

## Instalation

`*npm install*`

`*npm start*`

open [http://localhost:3000/](http://localhost:3000/)

## Heroku API fighter-app
[Heroku API-Fighter](https://fighter-nodejs.herokuapp.com/)

## Command 
* **GET**: */user*
Отримати всіх користувачів
[Example](https://fighter-nodejs.herokuapp.com/user)


* **GET**: */user/:id*
отримання одного користувача по ID
[Example](https://fighter-nodejs.herokuapp.com/user/7f280d2e-cb49-a37a-2674-0fd2833bdfa1)


* **POST**: */user*
створення користувача по даним переданим в тілі запиту

**Example**
```
{  
  name: "Name",
  health: 100,
  attack: 10,
  defense: 5,
  source: "http://www.fightersgeneration.com/hr.gif"
}
```
`name` - не менше ніж 3 символа.

`health, attack, defense` - число, не менше ніж 1, не від'ємне.

`source` - RFC 3986 URI.

* **PUT**: */user/:id*
обновлення користувача по даним переданим в тілі запиту

**Example**
```
{  
  name: "Name",
  health: 100,
  attack: 10,
  defense: 5,
  source: "http://www.fightersgeneration.com/hr.gif"
}
```

* **DELETE**:*/user/:id*
видалення одного користувача по ID

## 💻Developed by *Marian Muryn*

### License

This project is licensed under the MIT License

const fs = require('fs');
const file = 'fighters.json';

const getUsersFromFile = () => {    
    const users = fs.readFileSync(file, "utf-8")
    return JSON.parse(users);
}

const saveUsersToFile = data => {
    const writeFS = fs.writeFileSync(file, JSON.stringify(data, null, 4));
    return writeFS;
}

module.exports = {
    getUsersFromFile,
    saveUsersToFile
}
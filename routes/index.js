const express = require('express');
const router = express.Router();

const { validationResult } = require('express-validator/check');
// const { getUsers, createUser } = require('../repositories/user.repository');
const { checkValidData } = require('../services/validata.service');
const { getUsersById, updateUser, isExistUser, deleteUser, getAllUsers, createUser } = require('../services/user.service');  

router.get('/user', function(req, res,) {
  const result = getAllUsers();

  res.status(200).json(result);
});

router.get('/user/:id', (req, res) => {  
  if(!isExistUser(req.params.id)) {
    return res.status(404).json({ errors: 'User not found' });
  }
  const getUserId = getUsersById(req.params.id);

  res.send(getUserId)
});

router.post(
    '/user',
    checkValidData(),
    (req, res) => {      
      const errors = validationResult(req);
      if (!errors.isEmpty()) {        
        return res.status(400).json({ errors: errors.array() });
      }

      let user = {
        _id: null,
        name: req.body.name,
        health: req.body.health,
        attack: req.body.attack,
        defense: req.body.defense,
        source:  req.body.source,
      };

      const result = createUser(user);

      res.status(200).json({_id: result});
});

router.put('/user/:id',
    checkValidData(),
    (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {        
        return res.status(400).json({ errors: errors.array() });
      }

      if(!isExistUser(req.params.id)) {
        return res.status(404).json({ errors: 'User not found' });
      }

      const updateData = {
        name: req.body.name,
        health: req.body.health,
        attack: req.body.attack,
        defense: req.body.defense,
        source:  req.body.source
      }

      updateUser(req.params.id, updateData);

      res.sendStatus(200);
});

router.delete('/user/:id', (req, res) => {
  if(!isExistUser(req.params.id)) {
    return res.status(404).json({ errors: 'User not found' });
  }
  deleteUser(req.params.id);
  
  res.sendStatus(200)
});

module.exports = router;
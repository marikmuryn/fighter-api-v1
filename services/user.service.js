const { getUsersFromFile, saveUsersToFile } = require('../repositories/user.repository');
const guid = require('guid');

const getAllUsers = () => {
    return getUsersFromFile()
}

const createUser = (obj) => {
    const users = getUsersFromFile()
    obj._id = guid.create()
    users.push(obj);

    saveUsersToFile(users);
    return obj._id
}

const getUsersById = (id) => {    
  const users = getUsersFromFile()
  let user = users.find( user => { return user._id === id });
  return user;
}

const updateUser = (id, obj) => {
    const users = getUsersFromFile();
    const userIndex = users.findIndex( user => { return user._id == id });
    if(userIndex >= 0) {
        users[userIndex].name = obj.name;
        users[userIndex].health = obj.health;
        users[userIndex].attack = obj.attack;
        users[userIndex].defense = obj.defense;
        users[userIndex].source = obj.source;
    } 
    return saveUsersToFile(users);
}

const deleteUser = (id) => {
    const users = getUsersFromFile();
    const newArray = users.filter( users => { return users._id !== id });
    return saveUsersToFile(newArray)
} 

const isExistUser = (id) => {
    const users = getUsersFromFile();
    const userIndex = users.findIndex( user => { return user._id === id });
    return userIndex >= 0;
}

module.exports = {
    getUsersFromFile,
    saveUsersToFile,
    getUsersById,
    updateUser,
    isExistUser,
    deleteUser,
    getAllUsers,
    createUser
};
const { check } = require('express-validator/check');

const checkValidData = () => {
  const checked = [
    check('name')
      .exists()
      .isLength({min: 3})
      .escape()
      .trim()
      .withMessage('That name doesn‘t look right'),
    check('health')
      .isNumeric()
      .isInt()
      .isLength({min: 1})
      .trim()
      .withMessage('That health doesn‘t look right'),
    check('attack')
      .isNumeric()
      .isInt()
      .isLength({min: 1})
      .trim()
      .withMessage('That attack doesn‘t look right'),
    check('defense')
      .isNumeric()
      .isInt()
      .isLength({min: 1})
      .trim()
      .withMessage('That defense doesn‘t look right'),
    check('source')
      .isURL()
      .trim()
      .withMessage('That source doesn‘t look right'),
  ]

  return checked;
}



module.exports = {
    checkValidData
}
